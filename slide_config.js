var PRESENTERS = {
    'mblomdahl': {
        name: 'Mats Blomdahl',
        company: 'Utvecklare, Stormfors',
        gplus: 'http://gplus.to/mblomdahl',
        twitter: '@mblomdahl',
        www: 'http://www.stormfors.se',
        github: 'https://bitbucket.org/mblomdahl'
    },
    'forsman': {
        name: 'Johan Forsman',
        company: 'Assistent, Stormfors',
        gplus: 'http://plus.google.com/114583145038361441490',
        twitter: '@johan_forsman',
        www: 'http://johanhforsman.com',
        github: 'http://bitbucket.org/johan_forsman'
    }
};

var SLIDE_CONFIG = new function() {

    var slideConfigs = [];
    var pathToConfigMap = {};
    var filenameToConfigMap = {};

    this.addConfig = function(config) {
        var slideIndex = slideConfigs.length;
        slideConfigs.push(config);

        if (config.path) {
            pathToConfigMap[config.path] = slideIndex;
        }

        if (config.filename) {
            filenameToConfigMap[config.filename] = slideIndex;
        }
    };

    this.fromFilename = function(filename) {

        var slideIndex = filenameToConfigMap[filename];

        return slideIndex >= 0 ? slideConfigs[slideIndex] : null;
    };

    this.fromPath = function(path) {

        var slideIndex = pathToConfigMap[path];

        return slideIndex >= 0 ? slideConfigs[slideIndex] : null;
    };
};

SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: undefined,

    // Slide settings
    settings: {
        title: 'HTML 5<br>Fast-Track',
        subtitle: 'Subtitle Goes Here',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['forsman']
    ],

    // Slide filename
    filename: 'template.html'
});

SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-1/presentation',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Presentation',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s1_presentation.html'
});
SLIDE_CONFIG.addConfig({
        // Slade path
        path: '/session-1/io-2012-slides',

        // Slide settings
        settings: {
            title: 'Google HTML5 Slide Deck',
            subtitle: 'Hur den funkar',
            //eventInfo: {
            //  title: 'Google I/O',
            //  date: '6/x/2013'
            //},
            useBuilds: true, // Default: true. False will turn off slide animation builds.
            usePrettify: true, // Default: true
            enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
            enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
            //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
            favIcon: 'images/stormfors.png',
            fonts: [
                'Open Sans:regular,semibold,italic,italicsemibold',
                'Source Code Pro'
            ]
            //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
        },

        // Author information
        presenters: [
            PRESENTERS['forsman']
        ],

        // Slide filename
        filename: 'html5_fast-track_s1_google-slide-deck-101.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-1/css3-intro',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Presentation och styling med CSS3',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s1_css3-properties.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-1/html5-markup',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Ny och ändrad HTML-<em>mark-up</em> i HTML5',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s1_html5-elements.html'
});

SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-2/presentation',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Presentation',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s2_presentation.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-2/intermediate-javascript',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Intermediate JavaScript (och lite Node.js)',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s2_intermediate-javascript.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-2/browser-apis',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'Browser APIs #1',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s2_browser-apis-1.html'
});

// TODO(mats.blomdahl@gmail.com): Komplettera ASAP.
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-2/css3-mobile',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'CSS3 för mobila enheter (Media Queries & <em>Responsive</em>)',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s2_css3-mobile.html'
});

SLIDE_CONFIG.addConfig({

    // Slide pathname
    path: '/session-3/web-dev-toolbox',

    // Slide settings
    settings: {
        title: 'Verktygslådan',
        subtitle: ' – Google Chrome, Git, Node.js & JetBrains WebStorm',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s3_web-dev-toolbox.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-3/the-story-of-touch',

    // Slide settings
    settings: {
        title: 'Fallstudie 1',
        subtitle: 'The Story of Touch – 10 min',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s3_the-story-of-touch.html'
});
SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: '/session-3/js-libraries',

    // Slide settings
    settings: {
        title: 'HTML5 Fast-Track',
        subtitle: 'CSS3 för mobila enheter (Media Queries & <em>Responsive</em>) – 15 min',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['mblomdahl']
    ],

    // Slide filename
    filename: 'html5_fast-track_s3_js-libraries.html'
});

SLIDE_CONFIG.addConfig({
    // Slide pathname
    path: undefined,

    // Slide settings
    settings: {
        title: 'Centuri',
        subtitle: 'Workshow varumärke 2012.10',
        //eventInfo: {
        //  title: 'Google I/O',
        //  date: '6/x/2013'
        //},
        useBuilds: true, // Default: true. False will turn off slide animation builds.
        usePrettify: true, // Default: true
        enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
        enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
        //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
        favIcon: 'images/stormfors.png',
        fonts: [
            'Open Sans:regular,semibold,italic,italicsemibold',
            'Source Code Pro'
        ]
        //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
    },

    // Author information
    presenters: [
        PRESENTERS['forsman']
    ],

    // Slide filename
    filename: 'centuri_slides.html'
});
